# BATISTATO

> Un outil pour la connaissance du parc bâti des territoires d’Île-de-France


## Installation

Pour installer les packages R nécessaires au lancement de l'application, utiliser : 

```r
renv::restore()
```

Si renv ne fonctionne pas, le désactiver avec `renv::deactivate()` et installer les packages avec : 

```r
install.packages(c(
  "rsconnect", "apexcharter", "bsicons", "bslib", "config", "dplyr", 
  "forcats", "glue", "htmltools", "janitor", "leaflet", "markdown", 
  "phosphoricons", "R.utils", "reactable", "readr", "rmarkdown", 
  "sever", "sf", "shiny", "shinybusy", "shinyWidgets", "stringr", 
  "tidyr", "writexl", "knitr", "classInt", "rlang", "scales", "colorspace", 
  "stringi", "tools", "renv", "data.table", "readODS", "tibble"
))
```


## Lancement de l'application

L'application se lance via le script [app.R](app.R), soit :

* en ouvrant le script et en cliquant sur le bouton "Run App" dans RStudio
* en tappant `shiny::runApp()` dans la console R (une fois situé dans le projet RStudio)



## Déploiement de l'application

L'application peut être déployée via l'interface de RStudio (bouton bleu en haut à droite lorsque le script 'app.R' est ouvert), ou bien avec le code suivant dans la console R :

```r
rsconnect::deployApp(
  appName = "batistato-dev", # ou "batistato" -> nom dans l'URL
  account = "ssm-ecologie",
  appFiles = c(
    "app.R",
    "config.yml",
    "fonctions/",
    "modules/",
    "datas/",
    "www/"
  ),
  forceUpdate = TRUE
)
```


## Structure du projet

Le projet contient les répertoires et fichiers suivant :


* `app.R` : Script principal permettant de lancer l'application.

* `config.yml` : Permet de modifier le numéro de la version, le titre de l'application, le sous-titre de l'application, le nom de l'application, le courriel de contact, l'organisme, la couleur principale de l'application et les titres des onglets.

* `README.md` : Informations générales sur l'application.

* `documents/` : Contient tous les fichiers markdown permettant de générer les pages affichées dans l'application.

* `fonctions/` : Contient toutes les fonctions appelées dans l'application. 

* `modules/` : Contient tous les modules shiny utilisés dans l'application.

* `inputs/` : Contient les fichiers sources .csv contenant les données. A mettre à jour (en remplaçant les fichiers) lorsque de nouvelles données sont disponibles, puis relancer les scripts de préparation  des données (dans `scripts/`) pour générer les nouveaux fichiers .rds (situées dans `datas/`.

* `datas/` : Contient tous les fichiers de données préparés pour être utilisés dans l'application au format `.rds`.

* `scripts/` : Contient les scripts R permettant de préparer les données depuis les fichiers d'entrées (`inputs/`)

* `renv/` : Dossier généré automatiquement par R, contient des informations sur la version de R utilisée ainsi que des packages dans la librairie. Contient le script activate.R qui permet d'obtenir la marche à suivre en cas de problèmes liés à la version de R.

* `rsconnect/` : Dossier généré automatiquement par shinyapps, lors de la mise en ligne de l'application. Contient des informations à propos de la version de l'application mise en ligne.

* `www/` : Contient les sous-dossiers suivants :
    + `css/` : Contient les scripts fonts.css et styles.css. Ce sont les scripts à modifier si l'on veut modifer respectivement la police d'écriture et le style visuel de l'application.
    + `favicons/` : logo affiché dans l'onglet des navigateurs.
    + `images/` : Contient les images et logos affichés dans l'application.
    + `js/` : Contient les scripts JavaScript de l'application.
    + `documents/` : fichiers PDF téléchargeables dans l'application.



## Procédure à suivre pour...

### Modifier le titre de l'application, le numéro de version de l’application ou l’adresse mail de contact

Modifier les informations à partir du fichier config.yml.


### Modifier la police de caractère

Remplacer les polices dans le dossier www/fonts puis dans le script www/css/fonts.css.
La fonte Marianne a été téléchargée ici : https://www.systeme-de-design.gouv.fr/elements-d-interface/fondamentaux-de-l-identite-de-l-etat/typographie/


### Modifier le style des fonds de cartes

Remplacer les fonds de carte dans les fonctions `ajouter_tuiles_ign_orthophotos()` et `ajouter_tuiles_ign_plan_v2` du script [cartes_utils.R](fonctions/cartes_utils.R).


### Remplacer les images

Les remplacer dans le dossier www/images puis respectivement dans les scripts des modules où elles sont utilisées (si le nom du fichier a été modifié).


### Remplacer les textes

Remplacer les fichiers Markdown appropriés dans le dossier [documents/](documents/).


### Changer la couleur d'un graphique

Les couleurs associées aux modalités des données sont définies dans la fonction [`couleurs_modalite()`](fonctions/couleurs.R)


### Mettre à jour les données

Voir [les données et scripts correspondants](datas/README.md).


### Mettre à jour les données cartographiques

Voir [les données et scripts correspondants](datas/README.md).

