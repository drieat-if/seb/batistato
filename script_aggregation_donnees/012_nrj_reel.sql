DO
$$
BEGIN

/* Le nom de la table bilan table_bilan_energie est à modifier dans tout le code */
/* Le nom de la table finale table_finale_energie_reel est à modifier dans tout le code */


	-- 1. CREATION DE LA TABLE
	

/*
		-- supprimer la table si elle existe
		drop table if exists table_finale_energie_reel;

		-- créer la table 
		create table table_finale_energie_reel (
			maille		varchar(8) NOT NULL,
			idcom 		varchar(5) ,
			siren_epci	varchar(254),
			dep_com		varchar(254),
			secteur		varchar(10),
			type_usage	varchar(10),
			type_bati	varchar(10),
			energie		varchar(4),
			conso		numeric,
			ges			numeric
		);
*/



	-- 2. CALCUL DES DONNEES NIVEAU EPCI
		-- supprimer la table temporaire si elle existe
		drop table if exists t_b_nrj_0;

		-- créer la table temporaire avec uniquement les variables nécessaires
		create temporary table t_b_nrj_0
		as 
		select case when siren_epci='200058790' then 'T9'
		when siren_epci='200057941' then 'T10'
		when siren_epci='200058097' then 'T7'
		when siren_epci='200057875' then 'T8'
		when siren_epci='200057966' then 'T2'
		when siren_epci='200058014' then 'T12'		
		when siren_epci='200057982' then 'T4'
		when siren_epci='200058006' then 'T11'		
		when siren_epci='200057867' then 'T6'	
		when siren_epci='200057974' then 'T3'
		when siren_epci='200057990' then 'T5'	
		when siren_epci='217500016' then 'T1'	
		else siren_epci end as siren_epci,
		case when siren_epci in ('200057941','200058097','200057875','200057966','200058014','200057982',
								 '200058006','200057867','200057974','200057990','217500016','200058790') then 1 else 0 end as mgp,
		conso_tot_mwh,
		CONSO_RES,
		CONSO_TER,
		CONSO_AGRI,
		CONSO_IND,
		CONSO_TR,
		GES_SCOPE12_RES,
	    GES_SCOPE12_TER,
        GES_SCOPE12_AGRI,
        GES_SCOPE12_IND,
        GES_SCOPE12_PROD_NRJ,
        GES_SCOPE12_DECH,
		GES_SCOPE12_TR,
		GES_SCOPE12_TR_AUTRES

		from table_bilan_energie;
		
	
		-- annee, secteur, conso
		
		drop table if exists t_b_nrj_1;
		create temporary table t_b_nrj_1
		as 
		select siren_epci, mgp, 'RES' as Secteur, CONSO_RES as Consommation,GES_SCOPE12_RES as Emission
		from t_b_nrj_0
			union
		select siren_epci, mgp, 'TER' as Secteur, CONSO_TER as Consommation, ges_scope12_ter as Emission
		from t_b_nrj_0	
			union
		select siren_epci, mgp, 'AGRI' as Secteur, CONSO_AGRI as Consommation,GES_SCOPE12_AGRI  as Emission
		from t_b_nrj_0
			union
		select siren_epci, mgp, 'IND' as Secteur, CONSO_IND as Consommation,GES_SCOPE12_IND as Emission
		from t_b_nrj_0
			union
		select siren_epci, mgp, 'TR' as Secteur, CONSO_TR as Consommation,GES_SCOPE12_TR as Emission
		from t_b_nrj_0
			union
		select siren_epci, mgp, 'PROD_NRJ' as Secteur, NULL as Consommation,GES_SCOPE12_PROD_NRJ as Emission
		from t_b_nrj_0
			union
		select siren_epci, mgp, 'DECH' as Secteur, NULL as Consommation,GES_SCOPE12_DECH as Emission
		from t_b_nrj_0
			union
		select siren_epci, mgp, 'TR_AUTRES' as Secteur, NULL as Consommation,GES_SCOPE12_TR_AUTRES as Emission
		from t_b_nrj_0
		;
		
		truncate table_finale_energie_reel;
		insert into table_finale_energie_reel
		select 'EPCI' as maille, NULL as idcom,siren_epci,NULL as dep_com, secteur,NULL as type_usage,NULL as type_bati,NULL as energie,coalesce(Consommation,0) as conso,coalesce(Emission,0) as ges
		from t_b_nrj_1;

	-- 3. REGROUPEMENT PAR MGP ET REGION


		-- DEP 75
		drop table if exists t_bat_1;
		insert into table_finale_energie_reel
		select 'DEP' as maille, NULL as idcom,NULL as siren_epci,'75' as dep_com, secteur,NULL as type_usage,NULL as type_bati,NULL as energie,coalesce(Consommation,0) as conso,coalesce(Emission,0) as ges
		from t_b_nrj_1 where siren_epci='T1'
		;
		
		--MGP
		drop table if exists t_bat_1;
		create temporary table t_bat_1
		as select 'EPCI' as maille, secteur, sum(coalesce(Consommation,0)) as conso,sum(coalesce(Emission,0)) as ges
		from t_b_nrj_1
		where mgp =1
		group by maille,secteur;
		
		
		--REGION
		drop table if exists t_bat_2;
		create temporary table t_bat_2
		as select 'REGION' as maille,
		secteur, sum(coalesce(Consommation,0)) as conso,sum(coalesce(Emission,0)) as ges
		from t_b_nrj_1 b_t 
		group by maille, secteur;
		
		
		--REGROUPEMENT
		insert into table_finale_energie_reel
		select		maille, NULL as idcom,'200054781' as siren_epci,NULL as dep_com, secteur,NULL as type_usage,NULL as type_bati,NULL as energie,conso,ges from t_bat_1;
		
		insert into table_finale_energie_reel
		select maille, NULL as idocom,NULL as siren_epci, NULL as dep_com,secteur,NULL as type_usage,NULL as type_bati,NULL as energie,conso,ges from t_bat_2;

END
$$


/*EXPORT

select *
from table_finale_energie_reel 

*/