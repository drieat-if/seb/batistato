DO
$$
BEGIN

/* Le nom de la table d'origine table_bilan_energie_reel est à modifier dans tout le code */
/* Le nom de la table finale table_finale_energie_mix est à modifier dans tout le code */


/*
	-- 1. CREATION DE LA TABLE

		-- supprimer la table si elle existe
		drop table if exists table_finale_energie_mix;

		-- créer la table 
		create table table_finale_energie_mix (
			maille		varchar(8) NOT NULL,
			idcom 		varchar(5) ,
			siren_epci	varchar(254),
			dep_com		varchar(254),
			secteur		varchar(10),
			energie		varchar(4),
			conso		numeric
		);
*/
	-- 2. CALCUL DES DONNEES NIVEAU COMMUNE
		-- supprimer la table temporaire si elle existe
		drop table if exists t_b_nrj_mix_0;

		-- créer la table temporaire avec uniquement les variables nécessaires
		create temporary table t_b_nrj_mix_0
		as 
		select 
		case when siren_epci='200058790' then 'T9'
		when siren_epci='200057941' then 'T10'
		when siren_epci='200058097' then 'T7'
		when siren_epci='200057875' then 'T8'
		when siren_epci='200057966' then 'T2'
		when siren_epci='200058014' then 'T12'		
		when siren_epci='200057982' then 'T4'
		when siren_epci='200058006' then 'T11'		
		when siren_epci='200057867' then 'T6'	
		when siren_epci='200057974' then 'T3'
		when siren_epci='200057990' then 'T5'	
		when siren_epci='217500016' then 'T1'	
		else siren_epci end as siren_epci,
		case when siren_epci in ('200057941','200058097','200057875','200057966','200058014','200057982',
								 '200058006','200057867','200057974','200057990','217500016','200058790') then 1 else 0 end as mgp,
		CONSO_RES_gaz,
		CONSO_RES_elec,
		CONSO_RES_pp,
		CONSO_RES_bois,
		CONSO_RES_cu,
		CONSO_ter_gaz,
		CONSO_ter_elec,
		CONSO_ter_pp,
		CONSO_ter_bois,
		CONSO_ter_cu
		from table_bilan_energie_reel
		;

		
		-- secteur,type bati, age, conso
		
		drop table if exists t_b_nrj_mix_1;
		create temporary table t_b_nrj_mix_1
		as 
		select siren_epci,mgp,'RES' as Secteur,'GN' as energie,CONSO_RES_gaz as Consommation
		from t_b_nrj_mix_0
			union
		select siren_epci,mgp,'RES' as Secteur,'BOIS' as energie,CONSO_RES_bois as Consommation
		from t_b_nrj_mix_0	
			union
		select siren_epci,mgp,'RES' as Secteur,'PP' as energie,CONSO_RES_pp as Consommation
		from t_b_nrj_mix_0	
			union
		select siren_epci,mgp,'RES' as Secteur,'ELEC' as energie,CONSO_RES_elec as Consommation
		from t_b_nrj_mix_0	
			union
		select siren_epci,mgp,'RES' as Secteur,'CU' as energie,CONSO_RES_cu as Consommation
		from t_b_nrj_mix_0	
			union
		select siren_epci,mgp,'TER' as Secteur,'GN' as energie,CONSO_ter_gaz as Consommation
		from t_b_nrj_mix_0	
			union
		select siren_epci,mgp,'TER' as Secteur,'BOIS' as energie,CONSO_ter_bois as Consommation
		from t_b_nrj_mix_0		
			union
		select siren_epci,mgp,'TER' as Secteur,'PP' as energie,CONSO_ter_pp as Consommation
		from t_b_nrj_mix_0
			union
		select siren_epci,mgp,'TER' as Secteur,'ELEC' as energie,CONSO_ter_elec as Consommation
		from t_b_nrj_mix_0		
			union
		select siren_epci,mgp,'TER' as Secteur,'CU' as energie,CONSO_ter_cu as Consommation
		from t_b_nrj_mix_0
		;

		truncate table_finale_energie_mix;
		insert into table_finale_energie_mix
		select 'EPCI' as maille,NULL as idcom,siren_epci,null as dep_com,
		Secteur,energie,coalesce(Consommation,0) as conso
		from t_b_nrj_mix_1;

		

	-- 3. REGROUPEMENT PAR MGP ET REGION
		
		DELETE 
		FROM table_finale_energie_mix 
		WHERE maille<>'EPCI';
		
		-- Département 75
		insert into table_finale_energie_mix
		select 'DEP' as maille,NULL as idcom,NULL as siren_epci,'75' as dep_com,
		Secteur,energie,coalesce(Consommation,0) as conso
		from t_b_nrj_mix_1 where siren_epci='T1';

		--MGP
		drop table if exists t_bat_1;
		create temporary table t_bat_1
		as select 'EPCI' as maille, secteur,energie, sum(coalesce(Consommation,0)) as conso
		from t_b_nrj_mix_1
		where mgp =1
		group by maille,secteur,energie;
		
		
		--REGION
		drop table if exists t_bat_2;
		create temporary table t_bat_2
		as select 'REGION' as maille,
		secteur,energie, sum(coalesce(Consommation,0)) as conso
		from t_b_nrj_mix_1 b_t 
		group by maille,secteur,energie;
		
		
		--REGROUPEMENT
		insert into table_finale_energie_mix
		select		maille, NULL as idcom,'200054781' as siren_epci,NULL as dep_com, secteur,energie,conso from t_bat_1;
		
		insert into table_finale_energie_mix
		select maille, NULL as idcom,NULL as siren_epci, NULL as dep_com,secteur,energie,conso from t_bat_2;


END
$$

/*
EXPORT
select *
from table_finale_energie_mix where maille='DEP'

*/