DO
$$
BEGIN

/* Le nom de la table d'origine table_bilan_energie_normal est à modifier dans tout le code */
/* Le nom de la table finale table_finale_energie_evol est à modifier dans tout le code */


/*
	-- 1. CREATION DE LA TABLE

		-- supprimer la table si elle existe
		drop table if exists table_finale_energie_evol;

		-- créer la table 
		create table table_finale_energie_evol (
			maille		varchar(8) NOT NULL,
			idcom		varchar(5),
			siren_epci	varchar(254),
			dep_com		varchar(254),
			annee 		numeric,
			secteur		varchar(10),
			conso		numeric,
			ges 		numeric
		);
		
*/
	-- 2. CALCUL DES DONNEES NIVEAU EPCI
		-- supprimer la table temporaire si elle existe
		drop table if exists t_b_nrj_evol_0;

		-- créer la table temporaire avec uniquement les variables nécessaires
		create temporary table t_b_nrj_evol_0
		as 
		select case when siren_epci='200058790' then 'T9'
		when siren_epci='200057941' then 'T10'
		when siren_epci='200058097' then 'T7'
		when siren_epci='200057875' then 'T8'
		when siren_epci='200057966' then 'T2'
		when siren_epci='200058014' then 'T12'		
		when siren_epci='200057982' then 'T4'
		when siren_epci='200058006' then 'T11'		
		when siren_epci='200057867' then 'T6'	
		when siren_epci='200057974' then 'T3'
		when siren_epci='200057990' then 'T5'	
		when siren_epci='217500016' then 'T1'	
		else siren_epci end as siren_epci,
		case when siren_epci in ('200057941','200058097','200057875','200057966','200058014','200057982',
								 '200058006','200057867','200057974','200057990','217500016','200058790') then 1 else 0 end as mgp,
		annee,
		conso_tot_mwh,
		conso_res_mwh,
		conso_ter_mwh,
		ges_scope12,
		ges_scop12_res,
		ges_scop12_ter
		from table_bilan_energie_normal
		;
		
		-- annee, secteur, conso, ges
		
		drop table if exists t_b_nrj_evol_1;
		create temporary table t_b_nrj_evol_1
		as 
		select siren_epci, mgp, annee as annee, 'RES' as Secteur, conso_res_mwh as Consommation, ges_scop12_res as Emission
		from t_b_nrj_evol_0
			union
		select siren_epci, mgp,annee as annee,'TER' as Secteur, conso_ter_mwh as Consommation, ges_scop12_ter as Emission
		from t_b_nrj_evol_0	
		;

		truncate table_finale_energie_evol;
		insert into table_finale_energie_evol
		select 'EPCI' as maille, NULL as idcom,siren_epci,NULL as dep_com,
		annee, secteur,coalesce(Consommation,0) as conso, coalesce(Emission,0) as ges
		from t_b_nrj_evol_1;
		
		insert into table_finale_energie_evol
		select 'DEP' as maille, NULL as idcom,NULL as siren_epci,'75' as dep_com,
		annee, secteur,coalesce(Consommation,0) as conso, coalesce(Emission,0) as ges
		from t_b_nrj_evol_1
		where siren_epci='T1';
		
	
	-- 3. REGROUPEMENT PAR MGP ET REGION
				
		--MGP
		drop table if exists t_bat_1;
		create temporary table t_bat_1
		as select 'EPCI' as maille,
		annee, secteur, sum(coalesce(Consommation,0)) as conso, sum(coalesce(Emission,0)) as ges
		from t_b_nrj_evol_1
		where mgp =1
		group by maille,annee,secteur;
		
		
		--REGION
		drop table if exists t_bat_2;
		create temporary table t_bat_2
		as select 'REGION' as maille, annee,
		secteur, sum(coalesce(Consommation,0)) as conso, sum(coalesce(Emission,0)) as ges
		from t_b_nrj_evol_1 b_t 
		group by maille, secteur, annee;
		
		
		--REGROUPEMENT
		insert into table_finale_energie_evol
		select maille, NULL as idcom,'200054781' as siren_epci,NULL as dep_com, annee, secteur, conso, ges from t_bat_1;
		
		insert into table_finale_energie_evol
		select maille, NULL as idocom,NULL as siren_epci, NULL as dep_com, annee, secteur, conso, ges from t_bat_2;


END
$$


/*EXPORT

select *
from table_finale_energie_evol 
order by maille, annee, secteur

*/