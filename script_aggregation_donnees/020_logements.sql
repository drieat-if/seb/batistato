DO
$$
BEGIN

/* Le nom de la table finale table_communes_insee est à modifier dans tout le code */
/* Le nom de la table d'origine table_rpls_loi est à modifier dans tout le code */
/* Le nom de la table finale table_bat_logement_d est à modifier dans tout le code */
/* Le nom de la table finale table_bat_logement_nd est à modifier dans tout le code */
/* Le nom de la table finale table_bat_logement est à modifier dans tout le code */


/*
	--1. CREER LES TABLES
	-- supprimer la table si elle existe
	drop table if exists table_bat_logement_d;

	-- créer la table 
	create table table_bat_logement_d (
		maille character varying(8) NOT NULL,
		idcom 			varchar(5) ,
		siren_epci		varchar(254),
		dep_com			varchar(254),
		age				text,
		type_occupant text,
		type_occupant_d text,
		nombre			numeric,
		surface			numeric,
		secret_f		char(1)
	);
	
	-- supprimer la table si elle existe
	drop table if exists table_bat_logement_nd;

	-- créer la table 
	create table table_bat_logement_nd (
		maille character varying(8) NOT NULL,
		idcom 			varchar(5) ,
		siren_epci		varchar(254),
		dep_com			varchar(254),
		age				text,
		type_occupant text,
		nombre			numeric,
		surface			numeric,
		secret_f		char(1)
	);
	
	-- supprimer la table si elle existe
	drop table if exists table_bat_logement;

	-- créer la table 
	create table table_bat_logement (
		maille character varying(8) NOT NULL,
		idcom 			varchar(5) ,
		siren_epci		varchar(254),
		dep_com			varchar(254),
		age				text,
		type_occupant text,
		type_occupant_d text,
		nombre			numeric,
		surface			numeric,
		secret_f		char(1)
	);
	
*/
	-- 2. CALCULS A LA COMMUNE
	-- supprimer la table temporaire si elle existe
	-- Communes nécessaires
	drop table if exists t_communes;
	create temporary table t_communes
	as select code_insee AS idcom
	from table_communes_insee
	where dep_com <>'28'
	;
	
	-- Sélectionner les colonnes nécessaires
	drop table if exists t_batistato_logement_0;
	create temporary table t_batistato_logement_0 
	as select c.idcom,
	dteloc,loghlls,catpro3,ccthp,jannath,stoth,hlmsem,loghvac,rppo_rs
	from t_communes c
	left join x_ff_2023.fftp_2023_pb0010_local l
	on c.idcom=l.idcom
	where l.logh='t'-- logh='t'=logement
	;
	
	-- Créer les nouvelles colonnes
	drop table if exists t_batistato_logement_1;
	create temporary table t_batistato_logement_1 
	as 
	select idcom,
	case when jannath <= 1948 then 'Avant 1948'
		when jannath <= 1974 then '1949-1974'
		when jannath <= 1990 then '1975-1990'
		when jannath <= 2005 then '1991-2005'
		when jannath > 2005 then 'Après 2005'
		else 'Inconnu' end as age, -- catégorie d'années de construction
	case when dteloc='1' then 'I'
	when dteloc = '2' then 'C'
	else NULL end as Individuel_collectif, -- Individuel ou collectif
	case when hlmsem in ('5','6') then 'O'
	else 'N' end as HLM,
	case when ccthp='P' then 'O' else 'N' end as proprietaire,
	stoth,1 as nombre
	from t_batistato_logement_0 t;
	
	-- Grouper
	drop table if exists t_batistato_logement_2;
	create temporary table t_batistato_logement_2 
	as select idcom,age,individuel_collectif,proprietaire,sum(stoth) as surface,count(nombre) as nombre
	from t_batistato_logement_1
	where HLM='N' -- Enlever les HLM
	group by idcom,age,individuel_collectif,proprietaire;
	
	-- Type d'occupant
	drop table if exists t_batistato_logement_3;
	create temporary table t_batistato_logement_3 
	as select idcom,age,
	case when  individuel_collectif='I'  then 'Individuel privé'
		 when individuel_collectif='C' then 'Collectif privé'
		 else NULL end as type_occupant,
	case when individuel_collectif='I' and proprietaire='O' then 'Individuel privé - Propriétaire occupant'
		 when individuel_collectif='C' and proprietaire='O' then 'Collectif privé - Propriétaire occupant'
		 when individuel_collectif='I' and proprietaire='N' then 'Individuel privé - Locatif'
		 when individuel_collectif='C' and proprietaire='N' then 'Collectif privé - Locatif'
		 else NULL end as type_occupant_d,
	sum(surface) as surface,sum(nombre) as nombre
	from t_batistato_logement_2
	group by idcom,age,type_occupant,type_occupant_d
	;
	
	-- CALCULS HLM
	
	-- Sélectionner les colonnes nécessaires
	drop table if exists t_batistato_hlm_0;
	create temporary table t_batistato_hlm_0 
	as select depcom,typeconst,rpls_loi_r11_2023."mode",
	construct,surfhab
	from table_rpls_loi
	;
	
	-- Créer les nouvelles colonnes
	drop table if exists t_batistato_hlm_1;
	create temporary table t_batistato_hlm_1 
	as 
	select depcom as idcom,
	case when typeconst='I' then 'Parc social - Individuel' else 'Parc social - Collectif' end as type_occupant_d,
	case when construct <= 1948 then 'Avant 1948'
		when construct <= 1974 then '1949-1974'
		when construct <= 1990 then '1975-1990'
		when construct <= 2005 then '1991-2005'
		when construct > 2005 then 'Après 2005'
		else 'Inconnu' end as age, -- catégorie d'années de construction,
	surfhab,1 as nombre
	from t_batistato_hlm_0 t
	;
	
	-- Type d'occupant
	drop table if exists t_batistato_hlm_2;
	create temporary table t_batistato_hlm_2 
	as select idcom,age,type_occupant_d,
	sum(surfhab) as surface,sum(nombre) as nombre
	from t_batistato_hlm_1
	group by idcom,age,type_occupant_d
	;

	drop table if exists t_batistato_log;
	create temp table t_batistato_log
	as
	select idcom,age,type_occupant,type_occupant_d,nombre,surface
	from t_batistato_logement_3;
	
	insert into t_batistato_log
	select idcom,age,'Parc social' as type_occupant,type_occupant_d,nombre,surface
	from t_batistato_hlm_2
	;

 	-- 3. Creation de la table toutes combinaisons

	-- Table idcom
	drop table if exists t_idcom;
	create temp table t_idcom
	as select distinct idcom
	from t_batistato_log;
	
	-- Table age
	drop table if exists t_age;
	create temp table t_age
	as select distinct age 
	from t_batistato_log;

	-- Table différents types_occupant
	drop table if exists t_type_occupant;
	create temp table t_type_occupant
	as select distinct type_occupant,type_occupant_d
	from t_batistato_log;

	-- Combinaisons age, territoire, type_occupant
	drop table if exists t_combinaisons;
	create temp table t_combinaisons
	as select * from t_idcom
	cross join t_age
	cross join t_type_occupant;

	-- Table logement avec toutes les combinaisons
	drop table if exists t_tout_logement;
	create temp table t_tout_logement
	as select t0.idcom,
	t0.age,t0.type_occupant,t0.type_occupant_d,
	coalesce(t1.nombre,0) as nombre,coalesce(t1.surface,0) as surface
	from
	t_combinaisons t0
		left join t_batistato_log t1
			on  t0.idcom=t1.idcom
				and t0.age=t1.age
				and t0.type_occupant_d=t1.type_occupant_d
	;	


	-- 4. Peupler table table_bat_logement_d
	TRUNCATE table_bat_logement_d;
	INSERT into table_bat_logement_d
	select 'COMMUNE' as maille,idcom,NULL as siren_epci,NULL as dep_com,age,type_occupant,type_occupant_d,nombre,surface
	from t_tout_logement;
	
	-- 5. GROUPEMENT EPCI, DEPARTEMENT, REGION
	DELETE
	FROM table_bat_logement_d
	WHERE maille<>'COMMUNE';
	
	
	/*Communes et leurs EPCI hors département 28*/
	drop table if exists t_communes;
	create temporary table t_communes
	as select code_insee,siren_epci  -- EPCI
	from table_communes_insee
	where dep_com<>'28' and siren_epci is not null
	union
	select code_insee,id_ept as siren_epci -- EPT
	from table_communes_insee
	where dep_com<>'28' and id_ept is not null
	; -- CC du pays Houdanais, Région Ile de France et département 28
	
	drop table if exists t_dep;
	create temporary table t_dep
	as select code_insee,dep_com 
	from table_communes_insee
	where dep_com<>'28' 
	;

	drop table if exists t_bat_1;
	create temporary table t_bat_1
	as select 'EPCI' as maille, c.siren_epci as siren_epci,age,type_occupant,type_occupant_d,sum(nombre) as nombre,sum(surface) as surface
	from table_bat_logement_d b_0 
		left join t_communes c
			on b_0.idcom=c.code_insee
	group by maille,c.siren_epci,age,type_occupant,type_occupant_d;
 
 	drop table if exists t_bat_2;
	create temporary table t_bat_2
	as select 'DEP' as maille, c.dep_com as dep_com,age,type_occupant,type_occupant_d,sum(nombre) as nombre,sum(surface) as surface	
	from table_bat_logement_d b_0 
		inner join t_dep c
			on b_0.idcom=c.code_insee 
	group by maille,c.dep_com,age,type_occupant,type_occupant_d;
 
  	drop table if exists t_bat_3;
	create temporary table t_bat_3
	as select 'REGION' as maille,age,type_occupant,type_occupant_d,sum(nombre) as nombre,sum(surface) as surface
	from table_bat_logement_d b_t 
	group by maille,age,type_occupant,type_occupant_d;
	
	insert into table_bat_logement_d
	select maille,NULL as idcom,siren_epci,NULL as dep_com,age,type_occupant,type_occupant_d,nombre,surface 
	from t_bat_1;

	insert into table_bat_logement_d
	select maille,NULL as idcom,NULL as siren_epci,dep_com,age,type_occupant,type_occupant_d,nombre,surface
	from t_bat_2;

	insert into table_bat_logement_d
	select maille,NULL as idcom,NULL as siren_epci,NULL as dep_com,age,type_occupant,type_occupant_d,nombre,surface
	from t_bat_3;
	


 	-- 4. CREATION DE LA TABLE NON DETAILLEE
	truncate table_bat_logement_nd;
	insert into table_bat_logement_nd
	select maille,idcom,siren_epci,dep_com,age,type_occupant,
	sum(nombre) as nombre,sum(surface) as surface,NULL as secret_f
	from table_bat_logement_d
	group by maille,idcom,siren_epci,dep_com,age,type_occupant
	;
	

 	-- 5. SECRETISATION DE LA TABLE NON DETAILLEE
	/*Tables de groupement communes et EPCI/EPT*/
		/*Communes et leurs EPCI hors département 28*/
		drop table if exists t_communes;
		create temporary table t_communes
		as select code_insee,siren_epci  -- EPCI
		from table_communes_insee
		where dep_com<>'28' and siren_epci is not null
		union
		select code_insee,id_ept as siren_epci -- EPT
		from table_communes_insee
		where dep_com<>'28' and id_ept is not null
		; -- Région Ile de France hors département 28

		drop table if exists t_dep;
		create temporary table t_dep
		as select code_insee,dep_com 
		from table_communes_insee
		where dep_com<>'28' 
		;

	/*table_bat_logement*/
		/*Secret statistique si nombre concerné<11*/
		UPDATE table_bat_logement_nd
		SET secret_f = NULL;

		/*Secrétisation niveau 2 pour les EPCI/EPT*/	
		drop table if exists t_bat_2_s;
		create temporary table t_bat_2_s
		as
			select b_0.idcom, c.siren_epci,type_occupant,age,
			nombre as nombre,surface,case when nombre<11 then 1 else 0 end as secret_f_num
			from table_bat_logement_nd b_0 
				inner join t_communes c
					on b_0.idcom=c.code_insee
			where type_occupant <>'Parc social' and nombre>0-- Pas dee secrétisation pour le parc social
		order by siren_epci,type_occupant,age,nombre asc
		;
		
		
		drop table if exists t_bat_2_s1;
		create temporary table t_bat_2_s1
		as
		select siren_epci,idcom,type_occupant,age,nombre,secret_f_num,
		ROW_NUMBER() over(partition by siren_epci,type_occupant,age order by nombre) as secret_f_rank,
		sum(secret_f_num) over(partition by siren_epci,type_occupant,age) as secret_f_sum
		from t_bat_2_s
		order by siren_epci,type_occupant,nombre;
		
		/*Exemple :
		
		select * from t_bat_2_s1 where siren_epci='200023125' and type_occupant='Collectif privé' and age='1991-2005'
		select * from t_bat_2_s1 where siren_epci='200023125' and type_occupant='Collectif privé' and age='1949-1974'
		
		*/
		
		/*Secrétisation niveau 2 pour les départements*/	
		drop table if exists t_bat_2_s2;
		create temporary table t_bat_2_s2
		as
			select b_0.idcom, c.dep_com,type_occupant ,age,
			nombre as nombre,surface,case when nombre<11  then 1 else 0 end as secret_f_num
			from table_bat_logement_nd b_0 
				inner join t_dep c
					on b_0.idcom=c.code_insee
			where type_occupant<>'Parc social' and nombre>0
		order by dep_com,type_occupant ,nombre asc
		;

		drop table if exists t_bat_2_s3;
		create temporary table t_bat_2_s3
		as
		select dep_com,idcom,type_occupant,age,nombre,secret_f_num,
		ROW_NUMBER() over(partition by dep_com,type_occupant,age  order by nombre) as secret_f_rank,
		sum(secret_f_num) over(partition by dep_com,type_occupant,age) as secret_f_sum
		from t_bat_2_s2;

		/*Exemple :
		
		select * from t_bat_2_s3 where secret_f_sum=1 and type_occupant='Individuel privé' and age='Avant 1948'
		*/

		/*Somme à la commune*/
		drop table if exists t_bat_2_s4;
		create temporary table t_bat_2_s4
		as
			select idcom,
			age,type_occupant,
			nombre as nombre,surface,case when nombre<11  then 1 else 0 end as secret_f_num
			from table_bat_logement_nd b_0 
			where type_occupant <>'Parc social' and idcom is not null and nombre>0
		;	


		/*Secrétisation pour l'axe age > 1990*/
		drop table if exists t_bat_2_s5;
		create temporary table t_bat_2_s5
		as
			select b0.idcom,b0.age,b0.type_occupant
			from t_bat_2_s4 b_secret
				left join table_bat_logement_nd b0
					on b_secret.idcom=b0.idcom
						and b_secret.type_occupant=b0.type_occupant
						and (b_secret.age='1991-2005' and b0.age='Après 2005'
								or b_secret.age='Après 2005' and b0.age='1991-2005')
			where b_secret.secret_f_num=1 and b0.idcom is not null
		;
		
		/* exemple:
		select * from t_bat_2_s5 where idcom='77180'
		*/

		/*Secrétisation pour l'axe age < 1990*/
		drop table if exists t_bat_2_s6;
		create temporary table t_bat_2_s6
		as select * from t_bat_2_s4
		where age not in ('1991-2005','Après 2005');
		
		drop table if exists t_bat_2_s7;
		create temporary table t_bat_2_s7
		as
		select idcom,type_occupant,age,nombre,secret_f_num,
		ROW_NUMBER() over(partition by idcom,type_occupant  order by nombre) as secret_f_rank,
		sum(secret_f_num) over(partition by idcom,type_occupant) as secret_f_sum
		from t_bat_2_s6;
		
		/* exemple:
		select * from t_bat_2_s7 where idcom='77180'
		select * from t_bat_2_s7 where idcom='77005'
		*/
	

		/*Union de tous les types de secrétisation*/
		drop table if exists t_bat_2_s8;
		create temporary table t_bat_2_s8
		as
		SELECT idcom,type_occupant,age
		FROM t_bat_2_s1
		WHERE secret_f_sum=1 -- secret niveau 2 : somme à l'EPCI
		and secret_f_rank = 2
		union
		SELECT idcom,type_occupant,age
		FROM t_bat_2_s3
		WHERE secret_f_sum=1 -- secret niveau 2 : somme au département
		and secret_f_rank = 2
		union
		SELECT idcom,type_occupant,age -- secret niveau 2 : age >1990
		from t_bat_2_s5
		union
		SELECT idcom,type_occupant,age -- secret niveau 2 : age<1990
		FROM t_bat_2_s7
		WHERE secret_f_sum=1 -- secret niveau 2 : somme au département
		and secret_f_rank = 2
		;

		update table_bat_logement_nd b
		set secret_f='2' 
		from t_bat_2_s8
		where b.idcom=t_bat_2_s8.idcom
								and b.age=t_bat_2_s8.age
								and b.type_occupant=t_bat_2_s8.type_occupant
								;
								
		UPDATE table_bat_logement_nd
		SET secret_f='2'
		WHERE  	left(idcom,2)='75'
		and type_occupant='Individuel privé'
		;	-- secrétisation de tout Paris individuel privé
		
		UPDATE table_bat_logement_nd
		SET secret_f='1'
		WHERE  	nombre<11 and nombre>0
		and type_occupant<>'Parc social'
		;	-- secret de niveau 1, si le nombre de logements concernés est inférieur à 11 

 	-- 6. SECRETISATION DE LA TABLE NON DETAILLEE
		UPDATE table_bat_logement_d
		SET secret_f = NULL;
	
		drop table if exists t_bat_d_s1;
		create temporary table t_bat_d_s1
		as
		select maille,dep_com,siren_epci,idcom,sum(case when nombre<11 and nombre>0 then 1 else 0 end) as secret_f_sum
		from table_bat_logement_d
		where type_occupant='Individuel privé'
		group by maille,dep_com,siren_epci,idcom
		having 	sum(case when nombre<11 and nombre>0 then 1 else 0 end)>0;
		
		update table_bat_logement_d b
		set secret_f='2' 
		from t_bat_d_s1 t
		where b.idcom is not NULL and b.type_occupant='Individuel privé'
				and b.idcom=t.idcom 
		;
		
		update table_bat_logement_d b
		set secret_f='2' 
		from t_bat_d_s1 t
		where b.siren_epci is not NULL and b.type_occupant='Individuel privé'
				and b.siren_epci=t.siren_epci 
		;
		
		drop table if exists t_bat_d_s2;
		create temporary table t_bat_d_s2
		as
		select maille,dep_com,siren_epci,idcom,sum(case when nombre<11 and nombre>0 then 1 else 0 end) as secret_f_sum
		from table_bat_logement_d
		where type_occupant='Collectif privé'
		group by maille,dep_com,siren_epci,idcom
		having 	sum(case when nombre<11 and nombre>0 then 1 else 0 end)>0;
		
		update table_bat_logement_d b
		set secret_f='2' 
		from t_bat_d_s2 t
		where b.idcom is not NULL and b.type_occupant='Collectif privé'
				and b.idcom=t.idcom 
		;
		
		update table_bat_logement_d b
		set secret_f='2' 
		from t_bat_d_s2 t
		where b.siren_epci is not NULL and b.type_occupant='Collectif privé'
				and b.siren_epci=t.siren_epci 
		;
		
		UPDATE table_bat_logement_d
		SET secret_f='2'
		WHERE  	left(idcom,2)='75'
		and type_occupant='Individuel privé'
		;	-- secrétisation de tout Paris individuel privé
		
		
/*
		-- 7. EXPORT
		select maille,idcom,siren_epci,dep_com,type_occupant,age,
		case when secret_f in ('1','2') then NULL else nombre end as nombre,
		case when secret_f in ('1','2') then NULL else surface end as surface
		from table_bat_logement_nd
		
		select maille,idcom,siren_epci,dep_com,type_occupant,type_occupant_d,age,
		case when secret_f in ('1','2') then NULL else nombre end as nombre,
		case when secret_f in ('1','2') then NULL else surface end as surface
		from table_bat_logement_d

*/


END
$$

