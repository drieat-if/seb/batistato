DO
$$
BEGIN

/* Le nom de la table finale table_communes_insee est à modifier dans tout le code */
/* Le nom de la table d'origine table_bilan_energie_reel est à modifier dans tout le code */
/* Le nom de la table finale table_finale_energie_age est à modifier dans tout le code */


/*
	-- 1. CREATION DE LA TABLE

		-- supprimer la table si elle existe
		drop table if exists table_finale_energie_age;

		-- créer la table 
		create table table_finale_energie_age (
			maille		varchar(8) NOT NULL,
			idcom 		varchar(5) ,
			siren_epci	varchar(254),
			dep_com		varchar(254),
			secteur		varchar(10),
			type_bati	varchar(10),
			age	varchar(10),
			conso		numeric
		);
*/
	-- 2. CALCUL DES DONNEES NIVEAU COMMUNE
		-- supprimer la table temporaire si elle existe
		drop table if exists t_b_nrj_age_0;

		-- créer la table temporaire avec uniquement les variables nécessaires
		create temporary table t_b_nrj_age_0
		as 
		select insee_com as idcom,

		CONSO_RES_APP_AV46,
		CONSO_RES_APP_46_70,
		CONSO_RES_APP_71_90,
		CONSO_RES_APP_91_05,
		CONSO_RES_APP_AP05,
		CONSO_RES_MI_AV46,
		CONSO_RES_MI_46_70,
		CONSO_RES_MI_71_90,
		CONSO_RES_MI_91_05,
		CONSO_RES_MI_AP05
		from table_bilan_energie_reel
		;

		
		-- secteur,type bati, age, conso
		
		drop table if exists t_b_nrj_co;
		create temporary table t_b_nrj_co
		as 
		select idcom,'RES' as Secteur,'APP' as type_bati,'Avant 1946' as age,CONSO_RES_APP_AV46 as Consommation
		from t_b_nrj_age_0
			union
		select idcom,'RES' as Secteur,'APP' as type_bati,'1946-1970' as age,CONSO_RES_APP_46_70 as Consommation
		from t_b_nrj_age_0	
			union
		select idcom,'RES' as Secteur,'APP' as type_bati,'1971-1990' as age,CONSO_RES_APP_71_90 as Consommation
		from t_b_nrj_age_0	
			union
		select idcom,'RES' as Secteur,'APP' as type_bati,'1991-2005' as age,CONSO_RES_APP_91_05 as Consommation
		from t_b_nrj_age_0	
			union
			select idcom,'RES' as Secteur,'APP' as type_bati,'Après 2005' as age,CONSO_RES_APP_AP05 as Consommation
		from t_b_nrj_age_0	
			union
		select idcom,'RES' as Secteur,'MI' as type_bati,'Avant 1946' as age,CONSO_RES_MI_AV46 as Consommation
		from t_b_nrj_age_0		
			union
		select idcom,'RES' as Secteur,'MI' as type_bati,'1946-1970' as age,CONSO_RES_MI_46_70 as Consommation
		from t_b_nrj_age_0
			union
		select idcom,'RES' as Secteur,'MI' as type_bati,'1971-1990' as age,CONSO_RES_MI_71_90 as Consommation
		from t_b_nrj_age_0
			union
		select idcom,'RES' as Secteur,'MI' as type_bati,'1991-2005' as age,CONSO_RES_MI_91_05 as Consommation
		from t_b_nrj_age_0
			union
		select idcom,'RES' as Secteur,'MI' as type_bati,'Après 2005' as age,CONSO_RES_MI_AP05 as Consommation
		from t_b_nrj_age_0
		;

		truncate table_finale_energie_age;
		insert into table_finale_energie_age
		select 'COMMUNE' as maille,idcom,NULL as siren_epci,null as dep_com,
		Secteur,type_bati,age,coalesce(Consommation,0) as conso
		from t_b_nrj_co;
		

	-- 3. GROUPEMENT PAR EPCI, EPT, DEPARTEMENT
		
		DELETE 
		FROM table_finale_energie_age 
		WHERE maille<>'COMMUNE';

		/*Communes et leurs EPCI hors département 28*/
		drop table if exists t_communes;
		create temporary table t_communes
		as select code_insee,siren_epci  -- EPCI
		from table_communes_insee
		where dep_com<>'28' and siren_epci is not null
		union
		select code_insee,id_ept as siren_epci -- EPT
		from table_communes_insee
		where dep_com<>'28' and id_ept is not null
		; -- CC du pays Houdanais, Région Ile de France et département 28

		drop table if exists t_dep;
		create temporary table t_dep
		as select code_insee,dep_com 
		from table_communes_insee
		where dep_com<>'28' 
		;

		drop table if exists t_bat_1;
		create temporary table t_bat_1
		as select 'EPCI' as maille, c.siren_epci as siren_epci,
		secteur,type_bati,age,sum(conso) as conso
		from table_finale_energie_age b_0 
			left join t_communes c
				on b_0.idcom=c.code_insee
		group by maille,c.siren_epci,secteur,type_bati,age;

		drop table if exists t_bat_2;
		create temporary table t_bat_2
		as select 'DEP' as maille, c.dep_com as dep_com,
		secteur,type_bati,age,sum(conso) as conso
		from table_finale_energie_age b_0 
			inner join t_dep c
				on b_0.idcom=c.code_insee
		group by maille,c.dep_com,secteur,type_bati,age;

		drop table if exists t_bat_3;
		create temporary table t_bat_3
		as select 'REGION' as maille, 
		secteur,type_bati,age,sum(conso) as conso
		from table_finale_energie_age b_t 
		group by maille,secteur,type_bati,age;

		insert into table_finale_energie_age
		select maille,NULL as idcom,siren_epci,NULL as dep_com,secteur,type_bati,age,conso from t_bat_1;

		insert into table_finale_energie_age
		select maille,NULL as idcom,NULL as siren_epci,dep_com,secteur,type_bati,age,conso from t_bat_2;

		insert into table_finale_energie_age
		select maille,NULL as idcom,NULL as siren_epci,NULL as dep_com,secteur,type_bati,age,conso from t_bat_3;


END
$$

/*
EXPORT
select *
from table_finale_energie_age

*/