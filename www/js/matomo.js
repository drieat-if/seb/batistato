var _paq = (window._paq = window._paq || []);
/* tracker methods like "setCustomDimension" should be called before "trackPageView" */
_paq.push(["setDocumentTitle", "https://ssm-ecologie.shinyapps.io/batistato/"]);
_paq.push(["setDownloadClasses", ["LienTelecharg", "document"]]);
_paq.push(["trackPageView"]);
_paq.push(["enableLinkTracking"]);
(function() {
  var u = "//audience-sites.din.developpement-durable.gouv.fr/";
  _paq.push(["setTrackerUrl", u + "piwik.php"]);
  _paq.push(["setSiteId", "1635"]);
  var d = document,
    g = d.createElement("script"),
    s = d.getElementsByTagName("script")[0];
  g.type = "text/javascript";
  g.async = true;
  g.defer = true;
  g.src = u + "piwik.js";
  s.parentNode.insertBefore(g, s);
})();

function piwikTrackVideo(type, section, page, x1) {
  _paq.push(["trackEvent", "Video", "Play", page]);
}
/*
</script>
<noscript><p><img src="//audience-sites.din.developpement-durable.gouv.fr/piwik.php?idsite=$IDSITE}&rec=1&action_name={https://ssm-ecologie.shinyapps.io/batistato/}" style="border:0;" alt="" /></p></noscript>
*/

$(document).on("shiny:inputchanged", function(event) {
  if (event.name === "nav" || event.name === "accueil-territoire") {
    _paq.push(["trackEvent", "input", "updates", event.name, event.value]);
  }
});

