/*jshint
  browser:true,
  devel: true
*/
/*global LeafletWidget */


(function() {
  $(document).on("click", '[data-toggle="popover"]', function() {
    $(this).popover({sanitize: false, html: true});
    $(this).popover("show");
  });
  $(document).on("click", '.scroll-top', function() {
    $("html, body").animate({ scrollTop: 0 }, "fast");
    return false;
  });
  LeafletWidget.methods.addCustomContent = function(options) {
    (function(){
      var map = this;
      if (options.id) {
        var id = options.id;
        if (map[id]) {
          map[id].remove();
          delete map[id];
        }
        map[id] = L.control.custom(options);
        map.controls.add(map[id]);
      } else {
        map.customContent = L.control.custom(options);
        map.controls.add(map.customContent);
      }
    }).call(this);
  };
  LeafletWidget.methods.addZoom = function(options) {
    (function(){
      var map = this;
      if (map.hasOwnProperty("zoomControl")) {
        map.zoomControl.remove();
        L.control.zoom({ position: options.position }).addTo(map);
      }
      
    }).call(this);
  };
  LeafletWidget.methods.addEasyPrint = function(options) {
    (function(){
      var map = this;
      var printcarto = L.easyPrint(options).addTo(map);
      Shiny.addCustomMessageHandler("telecharger-carto", function(obj) {
        printcarto.printMap(obj.format, obj.fichier);
        Shiny.setInputValue("cartographie-spinner_show", true, {priority: "event"});
        map.on("easyPrint-finished", function (ev) {
          Shiny.setInputValue("cartographie-spinner_hide", true, {priority: "event"});
        }, 10000);
      });
    }).call(this);
  };
  $(document).on('shiny:inputchanged', function(event) {
    if (event.name === "tabs" && event.value === "portrait-de-territoire") {
      //console.log(event);
      const scrollSpy = new bootstrap.ScrollSpy(document.body, {target: '#onglet_1-menu-sections'});
    }
  });
  
  
  
  function checkPosition() {
    let scrollPos = 1000;
    const nav = document.querySelector('.scroll-top');
    let windowY = window.scrollY;
    if (windowY < scrollPos) {
      // Scrolling UP
      nav.classList.add("invisible");
      nav.classList.remove("visible");
    } else {
      // Scrolling DOWN
      nav.classList.add("visible");
      nav.classList.remove("invisible");
    }
    scrollPos = windowY;
  }
  function debounce(func, wait = 10, immediate = true) {
    let timeout;
    return function() {
      let context = this, args = arguments;
      let later = function() {
        timeout = null;
        if (!immediate) func.apply(context, args);
      };
      let callNow = immediate && !timeout;
      clearTimeout(timeout);
      timeout = setTimeout(later, wait);
      if (callNow) func.apply(context, args);
    };
  };

  $(document).on("scroll", debounce(checkPosition));
})();
