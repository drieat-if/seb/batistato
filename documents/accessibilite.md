## À propos de la prise en charge du navigateur web pour Batistato

Nous concevons Batistato en tenant compte des derniers navigateurs web. Nous vous recommandons d’utiliser la dernière version de l’un des navigateurs suivants.

* Apple Safari (Version 16.5)
* Google Chrome (Version 114.0.5735.110)
* Microsoft Edge (Version 114.0.1823.58)
* Mozilla Firefox (Version 114.0.2)

Si vous n’utilisez pas la dernière version d’un navigateur recommandé, ou si vous utilisez un navigateur qui ne figure pas dans la liste ci-dessus, Batistato ou certaines fonctionnalités peuvent ne pas fonctionner comme prévu, voire pas du tout.



## Accessibilité : non conforme

La DRIEAT Ile-de-France s’engage à rendre son service accessible, conformément à l’article 47 de la loi n° 2005-102 du 11 février 2005. Cette déclaration d’accessibilité s’applique à «https://ssm-ecologie.shinyapps.io/batistato/ ».


### État de conformité

Le site https://ssm-ecologie.shinyapps.io/batistato/ est non conforme avec le référentiel général d’amélioration de l’accessibilité pour les administrations (RGAA), car il n’existe aucun résultat d’audit en cours de validité permettant de mesurer le respect des critères. Les corrections des non-conformités sont en cours dans un projet de transformation graphique (intégration de la nouvelle charte graphique de l’Etat) et un audit sera établi afin d’appliquer les corrections nécessaires pour améliorer l’accessibilité au niveau fonctionnel. Aussi, la présente déclaration d’accessibilité sera mise à jour au plus vite.


### Résultat des tests

En l’absence d’audit de conformité il n’y a pas de résultats de tests.



### Contenus non accessibles

#### Non-conformité
En l’absence d’audit tous les contenus seront considérés comme non accessibles par hypothèse.


#### Dérogations pour charge disproportionnée
En l’absence d’audit aucune dérogation n’a été établie.


#### Contenus non soumis à l’obligation d’accessibilité
En l’absence d’audit aucun contenu n’a été identifié comme n’entrant pas dans le champ de la législation applicable.


#### Technologies utilisées pour la réalisation du site https://ssm-ecologie.shinyapps.io/batistato/
En l’absence d’audit aucune technologie n’a été utilisée.


#### Agents utilisateurs, technologies d’assistance et outils utilisés pour vérifier l’accessibilité
En l’absence d’audit aucun agent utilisateur et aucune technologie d’assistance n’ont été utilisés.


#### Les tests des pages web ont été effectués avec les combinaisons de navigateurs web et lecteurs d’écran suivants :
En l’absence d’audit aucune combinaison de navigateur et de lecteur d’écran n’a été utilisée.


#### Les outils suivants ont été utilisés lors de l’évaluation :
En l’absence d’audit aucun outil n’a été utilisé lors de l’évaluation.


#### Pages du site ayant fait l’objet de la vérification de conformité
En l’absence d’audit aucune page n’a fait l’objet de la vérification de conformité.



### Amélioration et contacts

Vous avez signalé au responsable du site internet un défaut d’accessibilité qui vous empêche d’accéder à un contenu ou à un des services du portail et vous n’avez pas obtenu de réponse satisfaisante.

* Écrire un message au [Défenseur des droits](https://formulaire.defenseurdesdroits.fr/)
* Contacter le délégué du [Défenseur des droits dans votre région](https://www.defenseurdesdroits.fr/saisir/delegues)
* Envoyer un courrier par la poste (gratuit, ne pas mettre de timbre) Défenseur des droits Libre réponse 71120 75342 Paris CEDEX 07
