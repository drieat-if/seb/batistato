## Mentions légales

### Service gestionnaire

Ministère de la Transition écologique et de la Cohésion des territoires.

Direction Régionale et Interdépartementale de l'Environnement, de l'Aménagement et des Transports d'Île-de-France (DRIEAT) - 27-29 rue Leblanc, 75015 PARIS

### Directeur de la publication

Jean-Marc PICARD, directeur adjoint de la DRIEAT

### Hébergement

-   Rstudio - plateforme Shinyapps <http://shinyapps.io/>
-   Le site est hébergé par le Ministère de la Transition écologique et de la Cohésion des territoires (secrétariat général/service du numérique/sous-direction des méthodes et services de plateforme/ département infrastructures et services) dont les serveurs se situent 4 Impasse Pierre Raymond, 33160 Saint-Médard-en-Jalles. SIREN : 110068012
-   Siège social : Ministère de la Transition écologique et de la Cohésion des territoires, hôtel Roquelaure, 246 Boulevard Saint-Germain, 75007 PARIS

### Droit d'auteur - Licence

Tous les contenus présents sur le site de la DRIEAT sont couverts par le droit d'auteur. Toute reprise est dès lors conditionnée à l'accord de l'auteur en vertu de l'article L.122-4 du Code de la Propriété Intellectuelle. Toutes les informations liées à cette application (données et textes) sont publiées sous licence ouverte/open licence v2 (dite licence Etalab) : quiconque est libre de réutiliser ces informations, sous réserve notamment, d'en mentionner la filiation. Tous les scripts « source » de l'application sont disponibles sous licence GPL-v3.

### Usage

-   Les utilisateurs sont responsables des interrogations qu'ils formulent ainsi que de l'interprétation et de l'utilisation qu'ils font des résultats. Il leur appartient d'en faire un usage conforme aux réglementations en vigueur et aux recommandations de la CNIL lorsque des données ont un caractère nominatif (loi n° 78.17 du 6 janvier 1978, relative à l'informatique, aux fichiers et aux libertés dite loi informatique et libertés).
-   Il appartient à l'utilisateur de ce site de prendre toutes les mesures appropriées de façon à protéger ses propres données et/ou logiciels de la contamination par d'éventuels virus circulant sur le réseau Internet. De manière générale, la DRIEAT décline toute responsabilité à un éventuel dommage survenu pendant la consultation du présent site.

### Evolution du service

La DRIEAT ajoute régulièrement des données, modifie l'interface les formulations sur la base des retours des usagers et des évolutions réglementaires et législatives. Elle se réserve le droit de faire évoluer le site sans information préalable ou préavis.

### Disponibilité du service

L'accès au site peut être suspendu sans information préalable ni préavis, notamment pour des raisons de maintenance. La DRIEAT se réserve également le droit de bloquer, sans information préalable ni compensation financière, les usages mettant en péril l'utilisation du logiciel par d'autres usagers.

### Traitement des données à caractère personnel

La DRIEAT s'engage à ne pas collecter des données à caractère personnel que l'internaute lui communique. Celles-ci sont confidentielles et ne seront utilisées que pour les besoins du service.

### Conception

Ce site a été conçu et développé par [dreamRs](https://www.dreamrs.fr/) pour la DRIEAT.

### Code source

Le code source de l'application est mis à disposition sur le [gitlab de Batistato](https://gitlab.com/drieat-if/seb/batistato).


### Suivi de l'usage du site

<iframe
  style="border: 0; height: 200px; width: 600px"
  src="https://audience-sites.din.developpement-durable.gouv.fr/optinout.php?language=fr&backgroundColor=ffffff&fontColor=484848&fontSize=0.7em&fontFamily=Arial%2C%20Verdana%2C%20Geneva%2C%20Helvetica%2C%20sans-serif"
></iframe>



