---
title: "FAQ"
---

## FAQ


### Les données sont-elles disponibles pour toutes les communes d’Ile-de-France, quelle que soit leur taille ?

Oui, les données sont disponibles pour toutes les communes, EPCI, EPT, et départements d’Ile-de-France. Cependant, certaines données étant issues des fichiers fonciers, elles sont secrétisées (et donc non disponibles) lorsqu’il y a moins de 11 logements (pour les logements), ou moins de 11 locaux (pour le tertiaire) concernés.  
D’autre part, pour la CC du Pays Houdanais, dont certaines communes sont situées hors Ile-de-France, seules les données des communes étant en Ile-de-France sont disponibles.

### Des données sur le parc tertiaire public sont-elles disponibles ?

BATISTATO inclut des données sur le parc public, mais elles sont moins précises et moins exhaustives que pour le parc privé. Le parc tertiaire privé est décrit notamment grâce aux fichiers fonciers ; le parc public n’étant pas soumis à la taxe foncière, la connaissance de ce parc est moins précise. Pour parer à ce biais, le CEREMA a ajouté des données concernant les établissements recevant du public vulnérable (ERPV). À terme, la plateforme OPERAT, qui recense les données bâtimentaires et de consommations des bâtiments assujettis à ce dispositif, devrait permettre d’avoir une meilleure vision de ce parc.

### Pourquoi faire un focus sur le parc bâti construit avant 1990 ?

La RT 1988 est la première réglementation thermique à imposer des critères de performance énergétique ambitieux. Nous pouvons donc considérer que l’ensemble des bâtiments livrés avant 1990 sont à rénover. Cela ne signifie pas pour autant qu’aucune réflexion sur la performance des bâtiments post-1990 ne doit être envisagée.

### Pour le parc tertiaire, pourquoi un zoom n’est réalisé que sur les bureaux et commerces ?

Les principales catégories d’activités en termes de surfaces en Ile-de-France sont les bureaux, les commerces, la santé, l’enseignement et le sport/ les établissements culturels. Les données concernant le parc public étant moins précises et moins exhaustives, et le nombre et les surfaces de bâtiments de bureaux et commerces privés étant déjà considérable, nous avons pris le parti d’analyser plus en détails ces deux catégories.

### Quelles différences avec l’outil Energif ROSE ?

L’outil Energif du ROSE décrit tous les secteurs (résidentiel, tertiaire, agricole et industriel), Batistato est axé sur la rénovation énergétique et les indicateurs associés. Batistato est plus détaillé sur certains axes d’analyse (distinction propriétaire/ locataire par exemple), et utilise d’autres bases de données, en particulier les fichiers fonciers pour la description du parc bâti.

### Quelles bases de données sont utilisées ?

Voir l'onglet "Methodologie".

### Quelles sont les perspectives de développement de l’outil ?

L’application BATISTATO n’est pas figée. Une mise à jour régulière des données sera réalisée en fonction des mises à jours des producteurs de données. Ces données étant mises à jour au mieux annuellement, BATISTATO suivra ce rythme.

De nouveaux indicateurs et graphiques pourront être développés en fonction des besoins identifiés par les utilisateurs. Vous pouvez faire des propositions via le formulaire de contact.


### Dans l’onglet cartographique, peut-on visualiser un territoire en particulier (EPCI/ Département) ?

On peut zoomer directement sur la carte pour visualiser un territoire, mais aujourd’hui on ne peut que sélectionner un EPCI ou un Département dans l’onglet de recherche. 

### Est-ce que les données utilisées sont en open source ?

Les données d’origines sont des données fermées (soit les fichiers fonciers, avec modélisation du CSTB). Mais les données agrégées sont ouvertes et sont publiées sur data.gouv.fr. On peut aussi télécharger les données via les tableaux sous les différents graphiques de l’outil. De plus, les codes d’agrégation sont également publiés sur le gitlab de BATISTATO.

### Est-ce que l’outil a une déclinaison au niveau national ?

Non, mais les codes sont libres d’accès, donc il est possible de se l’approprier et de l’adapter à un autre territoire. Si cela vous intéresse, veuillez-nous contacter via le formulaire de contact. Cependant, il faut avoir accès aux données et avoir quelqu’un pour assurer le suivi et adapter au niveau local.

### Est-il possible d’estimer les potentiels d’économie d’énergie et de réduction de GES ?

Non, Batistato n’a pas une approche économique. Néanmoins, il existe l’outil [FacETe](https://www.outil-facete.fr/){target="_blank"}, qui permet de calculer la facture énergétique du territoire. 

### Est-ce qu’il a des statistiques sur l’évolution du parc au regard de l’objectif de 700 000 rénovations global par an ? et Est-ce qu’on peut suivre les évolutions des taux des passoires thermiques ?

Non, Batistato est un outil de diagnostic et de visualisation à un moment donné. Ce n’est pas un outil de suivi des tendances, ou du suivi des politiques de rénovation énergétique des bâtiments en IDF.

En revanche, l’observatoire national de la rénovation énergétique (ONRE), publie régulièrement des statistiques et études sur la rénovation énergétique, notamment via le tableau de bord de suivi de la rénovation énergétique.

De plus la DRIEAT publie un rapport trimestriel « Les Chiffres du bâtiment Francilien » en collaboration avec la DRIHL. Pour le recevoir, il faut remplir ce [formulaire d’inscription](https://support.google.com/drive/answer/6283888/){target="_blank"}.

### Combien de temps a duré la création de l’outil ?

Batistato existe depuis 2014 pour la partie logement et 2018 pour la partie tertiaire. Aujourd’hui, il s’agit d’une version modernisée de l’outil qui était déjà existant. 
La modernisation de l’outil a duré environ 8 mois entre la sélection du prestataire et la livraison de l’outil. Si le projet est dupliqué, dans la mesure où les codes existent déjà, la prise en charge et l’adaptation de l’outil pourraient être plus rapide. 

### Peut-on faire un benchmark par région, avec une visualisation des communes les plus vertueuses en résidentiel et tertiaire avec des indicateurs d’énergie et de carbone ?

Batistato est un outil à l’échelle de la région IDF qui permet de faire un diagnostic du parc à une période donnée. Les communes les plus vertueuses en termes de politique ne sont pas identifiées.

### La mise à jour des données, est-elle automatisée ?

Non, la mise à jour des données se fait manuellement, avec en principe une fréquence d’une mise à jour par an. 
Par exemple, la structure des fichiers fonciers peut changer et peut demander un prétraitement avant la mise à jour des données.

### Peut-on participer à l’alimentation de Batistato en data?

Prendre contact via le formulaire de contact pour toute information sur ce sujet.
Concernant la partie logement cela ne sera pas nécessaire, car la description du parc actuel est assez fiable (sur la base des fichiers fonciers).
Concernant le parc tertiaire, la connaissance du parc n’est pas assez précise et reste non-exhaustive. Il y a beaucoup de pistes, mais pas d’idées concrètes aujourd’hui sur comment améliorer la connaissance du parc tertiaire en IDF. 

### Peut-on avoir la structure et les codes de l’outil afin de pouvoir le dupliquer à d’autres régions ? 

Oui, c’est tout à fait possible, pour cela veuillez prendre contact via le formulaire de contact. L’ensemble des codes est publié sur le GITLAB de BATISTATO.

### Comment associer l’outil à la définition des zones d’accélération des énergies renouvelables (ZA-ENR)? 

Pour le moment, BATISTATO est une description fine du bâti, et n’aborde pas le thème des productions d’énergie renouvelable.

### Où sont les mentions légales ?

Les mentions légales sont en bas de page de l’interface numérique de l’outil.

