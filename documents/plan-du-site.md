## Plan du site

* Accueil
  * Objectif de Batistato
* Portrait de territoire
  * Chiffres clés
  * Aperçu géographique
  * Logement
    * Description du parc bâti
    * Consommations d’énergie
    * Performance énergétique
  * Tertiaire
    * Description du parc bâti
    * Consommations d’énergie
* Cartographie
* Méthodologie
* Contexte
* Ressources complémentaires

