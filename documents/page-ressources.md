---
title: "Ressources complémentaires"
---

## Ressources complémentaires

### Outils franciliens

#### Observatoire des Objectifs de Développement Durable (DRIEAT)

Un outil développé par la DRIEAT IF pour savoir où se situe une commune ou EPCI en termes de **développement durable** – et plus précisément au regard des 17 objectifs de développement durable de l’ONU.

[drieat.ile-de-france.developpement-durable.gouv.fr](https://www.drieat.ile-de-france.developpement-durable.gouv.fr/observatoire-des-odd-une-appli-gratuite-pour-a12258.html){target="_blank"}

![](images/DRIEAT.png){width=50%}


#### ENERGIF (ROSE)

ENERGIF est une application de visualisation cartographique et de mise à disposition des données du Réseau d'Observation Statistique de l'Énergie et des émissions de gaz à effet de serre en Île-de-France (ROSE). Elle permet de comprendre et de suivre les spécificités territoriales de la région Île-de-France, en particulier pour ce qui concerne les consommations énergétiques par secteur d’activités économiques, par énergie et par usage, la production locale d’énergie renouvelable et de récupération et les émissions de gaz à effet de serre de l'inventaire AIRPARIF.

[roseidf.org](https://www.roseidf.org/outils-ressources/energif/){target="_blank"}

![](images/logo_airparif.png){width=15%}
![](images/ENERGIF.jpg){width=35%}
![](images/logo_rose.jpg){width=15%}

### Outils nationaux

#### À l’échelle du bâtiment

##### Base de Données Nationale des Bâtiments (BDNB) (CSTB)

La BDNB est une cartographie du parc de bâtiments existants, construite par croisement géospatial d’une vingtaine de bases de données issues d’organismes publics. Structurée à la maille s« bâtiment », elle contient une carte d’identité pour chacun des 20 millions de bâtiments, résidentiels ou tertiaires.

[base-de-donnees-nationale-des-batiments](https://www.data.gouv.fr/fr/datasets/base-de-donnees-nationale-des-batiments/){target="_blank"}

![](images/logo_cstb.jpg){width=15%}
![](images/BDNB.jpg){width=15%}



##### GO-RÉNOVE (CSTB)

Go-rénove permet de montrer à l’échelle d’un bâtiment des caractéristiques détaillées, pour aider à la rénovation thermique d’un logement. Il existe un portail pour les particuliers et un portail pour les bailleurs sociaux.

[particulier.gorenove](https://particulier.gorenove.fr/){target="_blank"}
[bailleur.gorenove](https://bailleur.gorenove.fr/){target="_blank"}

![](images/logo_cstb.jpg){width=15%}
![](images/GO RENOVE.png){width=10%}


##### Observatoire national des Bâtiments (URBS)

L'ONB est un géoservice innovant qui met à disposition de tous les données « ouvertes » sur la totalité des bâtiments résidentiels du territoire national.

[imope](https://www.imope.fr/onb.html){target="_blank"}

![](images/logo_URBS_fondtransparent.png){width=15%}
![](images/ONB.png){width=15%}


##### France Chaleur Urbaine (DRIEAT)

Un service public pour faciliter et accélérer les raccordements aux réseaux de chaleur.

[france-chaleur-urbaine](https://france-chaleur-urbaine.beta.gouv.fr/){target="_blank"}

![](images/DRIEAT.png){width=15%}
![](images/FCU_logo.png){width=15%}


##### Plateforme Coach Copro (Agence Parisienne du Climat)

Coachcopro est un service gratuit et indépendant mis à disposition des copropriétés pour initier leurs démarches de rénovation et entreprendre leurs futurs projets.

[coachcopro](https://www.coachcopro.com){target="_blank"}

![](images/coach_pro.png){width=15%}


#### À l’échelle d’un parc tertiaire public

##### Prioreno (Banque des Territoires)

Dispositif partenarial pour faciliter la décision de la rénovation des bâtiments publics. Cet outil fournit des données sur les consommations énergétiques des bâtiments publics du territoire.

![](images/PRIORENO.png){width=10%}
![](images/BdT.PNG){width=15%}


####  Pour aller plus loin...

##### CREBA

Centre de ressources pour la réhabilitation du bâti ancien.

![](images/CREBA.png){width=25%}

[rehabilitation-bati-ancien](https://www.rehabilitation-bati-ancien.fr/){target="_blank"}


##### MOOC Bâtiment durable

Différents MOOC sur la rénovation énergétique des bâtiments, dont:
* Rénovation performante - les clés de la réhabilitation énergétique
* MOOC Concevoir une réhabilitation énergétique responsable du bâti ancien

[mooc-batiment-durable](https://www.mooc-batiment-durable.fr/){target="_blank"}

![](images/mooc.png)





