
#' Page d'accueil de l'application
#'
#' @param request Requête HTTP (non utilisé pour le moment).
#'
#' @return La partie UI et la prtie Serveur à utiliser dans [shinyApp()] pour lancer l'application..
#' @export
#' @name application
application_ui <- function(request) {
  
  # Construction de l'interface
  ui <- fluidPage(
    title = config::get("titre"), # titre pour l'onglet du navigateur
    theme = bs_theme_drieat(),
    tags$head(
      tags$link(rel = "stylesheet", href = "css/styles.css"),
      tags$link(rel = "stylesheet", href = "css/fonts.css"),
      tags$script(src = "js/leaflet-easyPrint/bundle.js"),
      tags$script(src = "js/leaflet-controlCustom/Leaflet.Control.Custom.js"),
      tags$script(src = "js/script.js"),
      tags$script(src = "js/matomo.js"),
      favicons(),
      html_dependency_phosphor(),
      htmltools::findDependencies(apexchartOutput("apexcharts")),
      htmltools::findDependencies(reactableOutput("reactable"))
    ),
    # Pour activer la page de déconnexion
    useSever(),
    # Ecran de chargement au démarrage de l'application
    shinybusy::busy_start_up(
      loader = spin_epic("trinity-rings", color = "#FFF"),
      text = "Chargement...",
      mode = "timeout",
      timeout = 1500,
      color = "#FFF",
      background = config::get("couleur_principale")
    ),
    # Barre de chargement en haut de la page
    shinybusy::add_busy_bar(
      color = config::get("couleur_principale"), 
      height = "5px"
    ),
    # Indicateur de chargement en pleine page (pour charger portrait de territoire)
    shinybusy::add_busy_spinner(
      spin = "trinity-rings",
      color = config::get("couleur_principale"),
      position = "full-page",
      timeout = 1500,
      onstart = FALSE
    ),
    
    tags$noscript(
      tags$p(tags$img(
        src = "//audience-sites.din.developpement-durable.gouv.fr/piwik.php?idsite='1635'&rec=1&action_name='https://ssm-ecologie.shinyapps.io/batistato/'",
        style = "border: 0;",
        alt = ""
      ))
    ),
    
    # Menu pour naviguer dans les onglets
    tags$div(
      class = "row justify-content-xl-center application-header",
      tags$div(
        class = "col-xxl-8 mx-1",
        style = css(marginTop = "10px", position = "relative"),
        titre_application(),
        menu_application()
      )
    ),
    
    # Onglets de l'application
    tabsetPanel(
      id = "tabs",
      type = "hidden",
      header = faux_plafond(),
      tabPanelBody(
        "accueil",
        page_accueil_ui("accueil")
      ),
      tabPanelBody(
        url_texte(config::get("onglets")[1]),
        page_portrait_ui("onglet_1")
      ),
      tabPanelBody(
        url_texte(config::get("onglets")[2]),
        page_cartographie_ui("cartographie")
      ),
      tabPanelBody(
        url_texte(config::get("onglets")[3]),
        page_markdown_ui(id = "methodo")
      ),
      tabPanelBody(
        url_texte(config::get("onglets")[4]),
        page_markdown_ui("contexte")
      ),
      tabPanelBody(
        url_texte(config::get("onglets")[5]),
        page_markdown_ui("ressources")
      ),
      tabPanelBody(
        url_texte(config::get("onglets")[6]),
        page_markdown_ui("faq")
      )
    ),
    tags$div(class = "push")
  )
  tagList(ui, bas_de_page())
}

#' @export
#' @rdname application
application_server <- function(input, output, session) {

  # Page de deconnexion
  sever(
    html = tagList(
      tags$h2(
        class = "text-primary",
        config::get("titre")
      ),
      tags$p(
        "Votre session a pris fin, cliquez sur le bouton ci-dessous pour vous reconnecter",
        style = css(textAlign = "center")
      ),
      tags$button("Se reconnecter", onClick = "location.reload();", class = "btn btn-primary")
    ),
    bg_color = "#FFFFFF",
    color = config::get("couleur_principale")
  )

  # Navigation dans les onglets
  observeEvent(input$nav, {
    updateTabsetPanel(
      session = session, 
      inputId = "tabs", 
      selected = input$nav
    )
  })
  observeEvent(input$nav2, {
    updateRadioGroupButtons(
      session = session,
      inputId = "nav", 
      selected = input$nav2
    )
    hideDropMenu("nav2_btn_dropmenu")
  })

  res_accueil <- page_accueil_server("accueil")
  
  observeEvent(res_accueil$territoire_r(), {
    updateRadioGroupButtons(
      session = session,
      inputId = "nav",
      selected = "portrait-de-territoire"
    )
  }, ignoreInit = TRUE)

  
  # Modules des différents onglets
  page_portrait_server("onglet_1", territoire_r = res_accueil$territoire_r)

  page_cartographie_server("cartographie")

  page_markdown_server("methodo", md = "documents/page-methodologie.md")

  page_markdown_server("contexte", md = "documents/page-contexte.md")
  
  page_markdown_server("ressources", md = "documents/page-ressources.md")
  
  page_markdown_server("faq", md = "documents/faq.md")

  # Affichage des mentions légales
  observeEvent(input$mentions_legales, afficher_markdown("documents/mentions-legales.md"))

  # Affichage rapport accessibilite
  observeEvent(input$accessibilite, afficher_markdown("documents/accessibilite.md"))
  
  # Affichage plan du site
  observeEvent(input$plan_du_site, afficher_markdown("documents/plan-du-site.md"))
}

