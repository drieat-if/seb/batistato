
#' Fenetre de selection d'un territoire
#'
#' @param id ID du module
#' @param class Classe CSS pour contenu du bouton.
#' @param territoire_r Une fonction [shiny::reactive()] retournant le code du territoire initialement sélectionné.
#'
#' @return Une fonction [shiny::reactive()] retournant le code du territoire finalement sélectionné.
#' @export
#' @name module-fenetre-selection-territoire
fenetre_selection_territoire_ui <- function(id, class = "fs-1") {
  ns <- NS(id)
  tagList(
    tags$div(
      class = "text-center mt-1",
      actionButton(
        inputId = ns("menu_selection_territoire"),
        label = tags$div(
          class = class,
          uiOutput(outputId = ns("titre_territoire"), inline = TRUE),
          ph(
            "swap",
            height = "1em", 
            weight = "bold",
            title = "Sélectionner un nouveau territoire"
          ),
          tags$div(
            "(cliquez pour sélectionner un nouveau territoire)",
            class = "fst-italic fs-6"
          )
        ),
        class = "btn-outline-secondary border-0"
      )
    )
  )
}

#' @export
#' @name module-fenetre-selection-territoire
fenetre_selection_territoire_server <- function(id, territoire_r = reactive(75101)) {
  moduleServer(
    id,
    function(input, output, session) {
      
      ns <- session$ns
      
      rv <- reactiveValues(territoire = NULL)
      observeEvent(territoire_r(), rv$territoire <- territoire_r())
      
      # Titre territoire
      output$titre_territoire <- renderUI({
        req(rv$territoire)
        table_choix_territoire() %>%
          filter(value == rv$territoire) %>% 
          pull(label)
      })
      
      observeEvent(input$menu_selection_territoire, {
        showModal(modalDialog(
          tagList(
            "Sélection d'un territoire",
            tags$button(
              phosphoricons::ph("x", height = "2em", title = "Fermer"),
              class = "btn btn-link",
              style = css(
                border = "0 none", 
                position = "absolute",
                top = "5px", 
                right = "5px"
              ),
              `data-bs-dismiss` = "modal",
              `aria-label` = "Fermer"
            )
          ),
          easyClose = TRUE,
          fade = FALSE,
          tags$br(), tags$br(),
          shinyWidgets::addSpinner(
            reactableOutput(outputId = ns("territoire"), height = "500px"), 
            color = config::get("couleur_principale")
          ),
          # verbatimTextOutput(ns("test")),
          actionButton(
            inputId = ns("selectionner"),
            label = "Sélectionner ce territoire",
            class = "btn-outline-primary mt-1",
            width = "100%"
          ),
          footer = NULL
        ))
      })
      
      output$territoire <- renderReactable({
        reactable(
          data = table_choix_territoire()[c("description_recherche", "group")],
          columns = list(
            description_recherche = colDef(name = "Territoire"),
            group = colDef(name = "Niveau"),
            .selection = colDef(
              style = list(cursor = "pointer"),
              headerStyle = list(cursor = "pointer")
            )
          ),
          selection = "single",
          onClick = "select",
          searchable = TRUE,
          highlight = TRUE,
          outlined = TRUE,
          striped = TRUE, 
          pagination = FALSE,
          theme = reactableTheme(
            searchInputStyle = list(width = "100%"),
            rowStyle = list(cursor = "pointer")
          ),
          language = reactableLang(
            searchLabel = "Rechercher...",
            searchPlaceholder = "Rechercher un territoire",
            noData = "Aucun territoire"
          )
        )
      })

      # output$test <- renderPrint(input$territoire)
      
      observeEvent(input$selectionner, {
        territoire_selected <- reactive(getReactableState("territoire", "selected"))
        rv$territoire <- table_choix_territoire()$value[territoire_selected()]
        shiny::removeModal()
      })
      
      return(reactive(rv$territoire))
    }
  )
}





