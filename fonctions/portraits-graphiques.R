
#' Graphique si pas de données à visualiser
#'
#' @param texte Un texte expliquant le manque de données.
#'
#' @return Un graphique `apexchart`.
#' @export
#'
#' @examples
#' graphique_pas_de_donnees()
#' graphique_pas_de_donnees(list("Rien à", "voir ici..."))
graphique_pas_de_donnees <- function(texte = "Pas de données pour ce territoire.") {
  g <- apexchart(list(
    chart = list(
      type = "bar", 
      animations = list(enabled = FALSE)
    ),
    series = list(),
    xaxis = list(
      labels = list(show = FALSE)
    ),
    yaxis = list(
      labels = list(show = FALSE)
    ),
    noData = list(
      align = "center",
      text = texte,
      style = list(
        fontSize = "15px",
        color = "#706f6f"
      )
    )
  ), height = "320px", auto_update = FALSE)
  class(g) <- c(class(g), "apexempty")
  return(g)
}



#' Graphique représentant l'historique
#'
#' @param donnees Données à visualiser, doit contenir les colonnes : `annee`, `valeur`, `libgeo`, `libvar`.
#' @param couleur Couleur pour les barres.
#'
#' @return Un graphique `apexchart`.
#' @export
#'
#' @examples
#' obtenir_donnees_historique("91021", "pauv17", inclure_niveaux = c("epci", "region")) %>%
#'   graphique_historique(couleur = "#eb1c2d")
#' obtenir_donnees_historique("75101", "pauv17", inclure_niveaux = c("epci", "region")) %>%
#'   graphique_historique(couleur = "#eb1c2d")
#'
#' obtenir_donnees_historique(c("91021", "75101"), "pauv17", inclure_niveaux = "aucun") %>%
#'   graphique_historique(couleur = "#eb1c2d")
graphique_historique <- function(donnees, couleur) {
  n <- n_distinct(donnees$libgeo)
  if (hasName(donnees, "echelle")) {
    donnees <- donnees %>%
      mutate(
        echelle = factor(echelle, levels = c("com", "ept", "epci", "reg"))
      ) %>%
      arrange(echelle)
  }
  apex(
    data = donnees,
    mapping = aes(as.character(annee), valeur, fill = libgeo),
    type = "column",
    height = "320px"
  ) %>%
    ax_chart(defaultLocale = "fr", animations = list(enabled = FALSE)) %>%
    ax_colors(c(
      couleur,
      colorspace::lighten(rep(couleur, n), amount = seq(from = 0.25, to = 0.5, length.out = n))
    )) %>%
    ax_fill(opacity = 1) %>%
    ax_legend(show = TRUE) %>%
    ax_yaxis(
      title = list(
        text = trouver_indicateur_unite(donnees$libvar[1], type = "libelle"),
        style = list(color = "#172B4D", fontWeight = "normal", fontFamily = "Marianne")
      )
    ) %>%
    ax_tooltip(y = list(
      formatter = format_num(",", locale = "fr-FR", suffix = paste0(" ", trouver_indicateur_unite(donnees$libvar[1])))
    ))
}


