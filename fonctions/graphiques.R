
graphique_barres_empilees <- function(donnees,
                                      titre,
                                      sous_titre,
                                      variable_x,
                                      variable_y,
                                      groupe,
                                      label_y,
                                      surface = FALSE) {
  
  if (nrow(donnees) < 1 | identical(sum(donnees[[variable_y]], na.rm = TRUE), 0)) 
    return(graphique_pas_de_donnees())
  
  # Tooltip
  suffixe <- if (isTRUE(surface)) {
    " m²"
  } else {
    ""
  }
  # Graphique
  apex(
    data = donnees,
    type = "column",
    mapping = aes(x = !!sym(variable_x), y = !!sym(variable_y), fill = !!sym(groupe)), 
    auto_update = FALSE,
    height = 450
  ) %>% 
    ax_plotOptions(bar = bar_opts(
      columnWidth = "50%"
    )) %>%
    ax_chart(
      stacked = TRUE, 
      animations = list(enabled = FALSE), 
      defaultLocale = "fr"
    ) %>% 
    ax_labs_batistato(
      titre = titre,
      sous_titre = sous_titre,
      y = label_y
    ) %>% 
    ax_tooltip(
      y = list(
        formatter = JS(
          "function(value) {",
          "if (value === null) return ' s ';",
          "var locale = formatLocale(JSON.parse('{  \"decimal\": \",\",  \"thousands\": \"\\u00a0\",  \"grouping\": [3],  \"currency\": [\"\", \"\\u00a0\\u20ac\"],  \"percent\": \"\\u202f%\"}'));",
          sprintf("return '' + locale.format(',.2~f')(value) + '%s';", suffixe),
          "}"
        )
      )
    ) %>% 
    ax_legend(showForSingleSeries = TRUE) %>% 
    ax_yaxis(
      labels = list(formatter = format_num(format = ",.2~f", locale = "fr-FR"))
    ) %>% 
    ax_copyright_batistato()
}




# Graphique camembert -----------------------------------------------------


# Fonction graphique
graphique_camembert <- function(donnees,
                                titre,
                                sous_titre,
                                variable_x,
                                variable_y,
                                surface = FALSE,
                                suffix = NULL) {
  
  if (nrow(donnees) < 1 | all(is.na(donnees[[variable_y]]))) 
    return(graphique_pas_de_donnees())
  
  # Tooltip
  if (isTRUE(surface)) {
    format <- format_num(format = ",.2~f", locale = "fr-FR", suffix = " m²")
  } else {
    format <- format_num(format = ",.2~f", locale = "fr-FR", suffix = suffix)
  }
  # Graphique
  apexchart(auto_update = FALSE, height = 450) %>%
    ax_chart(
      type = "pie",
      animations = list(enabled = FALSE),
      defaultLocale = "fr",
      toolbar = list(show = TRUE)
    ) %>%
    ax_series2(as.list(donnees[[variable_y]])) %>% 
    ax_labels2(as.list(donnees[[1]])) %>% 
    ax_colors(unlist(couleurs_modalite(donnees[[variable_x]]), use.names = FALSE)) %>% 
    ax_labs_batistato(
      titre = titre,
      sous_titre = sous_titre
    ) %>% 
    ax_tooltip(
      y = list(
        formatter = format
      )
    ) %>% 
    ax_legend(position = "bottom") %>% 
    ax_legend(showForSingleSeries = TRUE) %>% 
    ax_copyright_batistato()
}



# Graphique barres --------------------------------------------------------

# Fonction graphique
graphique_barres <- function(donnees,
                             titre,
                             sous_titre,
                             variable_x,
                             variable_y,
                             label_y = NULL) {
  
  if (nrow(donnees) < 1) 
    return(graphique_pas_de_donnees())
  
  # Graphique
  apex(
    data = donnees,
    type = "column",
    mapping = aes(x = donnees[[variable_x]], y = donnees[[variable_y]]),
    height = calculer_hauteur_graphique_barres(donnees[[variable_x]])
  ) %>% 
    ax_labs_batistato(
      titre = titre,
      sous_titre = sous_titre,
      y = label_y
    ) %>% 
    ax_tooltip(
      y = list(
        formatter = JS("function(value) {return value + ' MWh';}"),
        title = list(
          formatter = JS("function() {return 'Consommation: ';}")
        )
      )
    ) %>% 
    ax_legend(showForSingleSeries = TRUE) %>% 
    ax_chart(defaultLocale = "fr") %>% 
    ax_copyright_batistato()
}



graphique_barres_horizontales <- function(donnees,
                                          titre,
                                          sous_titre,
                                          variable_x,
                                          variable_y,
                                          surface = FALSE,
                                          label_y = NULL,
                                          label_x = NULL,
                                          hauteur = NULL) {
  
  if (nrow(donnees) < 1 | identical(sum(donnees[[variable_y]], na.rm = TRUE), 0)) 
    return(graphique_pas_de_donnees())
  
  # Tooltip
  if (isTRUE(surface)) {
    format <- format_num(format = ",.2~f", locale = "fr-FR", suffix = " m²")
  } else {
    format <- format_num(format = ",.2~f", locale = "fr-FR")
  }
  if (is.null(hauteur))
    hauteur <- 200 + n_distinct(donnees[[variable_x]]) * 25
  # Graphique
  apexchart(
    auto_update = FALSE,
    height = hauteur
  ) %>%
    ax_chart(type = "bar", animations = list(enabled = FALSE), defaultLocale = "fr") %>%
    ax_dataLabels(enabled = FALSE) %>%
    ax_grid(yaxis = list(lines = list(show = FALSE)), xaxis = list(lines = list(show = TRUE))) %>%
    ax_plotOptions(bar = bar_opts(
      horizontal = TRUE,
      distributed = TRUE
      # barHeight = 50
    )) %>%
    ax_series(list(
      name = "name", 
      data = as.list(donnees[[variable_y]])
    )) %>%
    ax_xaxis(categories = as.list(donnees[[1]])) %>%
    ax_colors(unlist(couleurs_modalite(donnees[[variable_x]]), use.names = FALSE)) %>% 
    ax_labs_batistato(
      titre = titre,
      sous_titre = sous_titre,
      y = label_y,
      x = label_x
    ) %>% 
    ax_tooltip(
      y = list(
        formatter = format,
        title = list(
          formatter = JS("function() {return 'Valeur :';}")
        )
      )
    ) %>% 
    ax_yaxis(
      labels = list(
        formatter = formatter_label_2lignes()
      )
    ) %>%  
    ax_legend(showForSingleSeries = TRUE) %>% 
    ax_copyright_batistato()
}



graphique_barres_horizontales_empilees <- function(donnees,
                                                   titre,
                                                   sous_titre,
                                                   variable_x,
                                                   variable_y,
                                                   groupe,
                                                   surface = FALSE,
                                                   stackType = "normal",
                                                   cacher_labels = FALSE,
                                                   suffix = NULL,
                                                   label_y = NULL) {
  if (nrow(donnees) < 1) 
    return(graphique_pas_de_donnees())
  
  # Tooltip
  if (isTRUE(surface)) {
    format <- format_num(format = ",.2~f", locale = "fr-FR", suffix = " m²")
  } else {
    format <- format_num(format = ",.2~f", locale = "fr-FR", suffix = suffix)
  }
  # Graphique
  ax <- apex(
    data = donnees,
    type = "bar",
    mapping = aes(x = donnees[[variable_x]], y = donnees[[variable_y]], fill = donnees[[groupe]]),
    height = calculer_hauteur_graphique_barres(donnees[[variable_x]])
  ) %>% 
    ax_chart(stacked = TRUE, stackType = stackType, animations = list(enabled = FALSE), defaultLocale = "fr") %>% 
    # ax_plotOptions(bar = bar_opts(
    #   barHeight = 50
    # )) %>%
    ax_labs_batistato(
      titre = titre,
      sous_titre = sous_titre,
      y = label_y
    ) %>% 
    ax_tooltip(
      y = list(
        formatter = format
      )
    ) %>% 
    ax_legend(showForSingleSeries = TRUE) %>% 
    ax_xaxis(labels = list(formatter = format_num(format = ",.2~f", locale = "fr-FR"))) %>% 
    ax_copyright_batistato()
  
  if (isTRUE(cacher_labels)) {
    ax <- ax %>% 
      ax_xaxis(
        overwriteCategories = list(" ")
      )
  } else {
    NULL
  }
  
  return(ax)
}


calculer_hauteur_graphique_barres <- function(x) {
  180 + n_distinct(x) * 32
}

formatter_label_2lignes <- function(sep = " - ") {
  JS(
    "function(value) {",
    # "console.log(value);",
    "if (!(typeof value === 'string' || value instanceof String)) return value;",
    sprintf("const sep = /%s/g;", sep),
    "const found = value.match(sep);",
    "if (found===null || found.length < 1) return value;",
    sprintf(
      "return value.split('%s');",
      sep
    ),
    "}"
  )
}


#' Ajouter un copyright Batistato sur un graphique
#'
#' @param ax Un graphique apexcharts.
#'
#' @return Un graphique apexcharts.
#' @export
#'
#' @examples
ax_copyright_batistato <- function(ax) {
  ax_annotations(
    ax = ax,
    texts = list(
      list(
        x = "99.9%",
        y = "99.9%",
        text = paste0("\U00A9", "Batistato"),
        textAnchor = "end",
        appendTo = ".apexcharts-svg",
        foreColor = "#A5A4A5", 
        fontSize = "11px"
      )
    )
  )
}



#' Ajouter des titres / sous-titres avec la charte Batistato
#'
#' @param ax Un graphique apexcharts.
#' @param titre Le titre.
#' @param sous_titre Le sous-titre.
#' @param x Titre axe des abcisses.
#' @param y Titre axe des ordonnées.
#'
#' @return Un graphique apexcharts.
#' @export
#'
#' @examples
ax_labs_batistato <- function(ax,
                              titre = NULL,
                              sous_titre = NULL,
                              x = NULL,
                              y = NULL) {
  if (!is.null(titre)) {
    ax <- ax_title(
      ax = ax,
      text = titre,
      style = list(
        fontWeight = 700,
        fontSize = "14px",
        fontFamily = "Marianne",
        color = "#172B4D"
      )
    )
    
  }
  if (!is.null(sous_titre)) {
    ax <- ax_subtitle(
      ax = ax,
      text = sous_titre,
      style = list(
        fontWeight = 400,
        fontSize = "11px",
        fontFamily = "Marianne",
        color = "#172B4D"
      )
    )
  }
  if (!is.null(x)) {
    ax <- ax_xaxis(ax = ax, title = list(
      text = x,
      style = list(
        fontWeight = 400,
        fontSize = "12px",
        fontFamily = "Marianne"
      )
    ))
  }
  if (!is.null(y)) {
    ax <- ax_yaxis(ax = ax, title = list(
      text = y,
      style = list(
        fontWeight = 400,
        fontSize = "12px",
        fontFamily = "Marianne"
      )
    ))
  }
  ax
}



graphique_barres_horizontales_empilees_100 <- function(donnees,
                                                       titre,
                                                       sous_titre,
                                                       variable_x,
                                                       variable_y,
                                                       groupe,
                                                       surface = FALSE,
                                                       suffix = NULL,
                                                       label_y = NULL,
                                                       texte_pas_de_donnees = "Pas de données pour ce territoire.") { # categories = 2
  if (nrow(donnees) < 1) 
    return(graphique_pas_de_donnees(texte = texte_pas_de_donnees))
  
  # Tooltip
  if (isTRUE(surface)) {
    format <- format_num(format = ",.2~f", locale = "fr-FR", suffix = " m²")
  } else {
    format <- format_num(format = ",.2~f", locale = "fr-FR", suffix = suffix)
  }
  
  apex(
    data = donnees,
    type = "bar",
    mapping = aes(x = donnees[[variable_x]], y = donnees[[variable_y]], fill = donnees[[groupe]]),
    height = calculer_hauteur_graphique_barres(donnees[[variable_x]])
  ) %>%
    ax_chart(stacked = TRUE, stackType = "100%", animations = list(enabled = FALSE), defaultLocale = "fr") %>% 
    ax_dataLabels(enabled = TRUE) %>% 
    ax_xaxis(labels = list(formatter = format_num("~s", suffix = "%"))) %>% 
    # ax_plotOptions(bar = bar_opts(
    #   barHeight = "50%"
    # )) %>%
    ax_labs_batistato(
      titre = titre,
      sous_titre = sous_titre,
      y = label_y
    ) %>% 
    ax_tooltip(
      y = list(formatter = format)
    ) %>% 
    ax_colors_manual(couleurs_modalite(donnees[[groupe]])) %>% 
    ax_legend(showForSingleSeries = TRUE) %>% 
    ax_yaxis(
      labels = list(
        maxWidth = 500
      )
    ) %>% 
    ax_copyright_batistato()
}



graphique_radar <- function(donnees,
                            titre = NULL,
                            sous_titre = NULL,
                            variable_x,
                            variable_y,
                            suffix = NULL,
                            label_y = NULL) {
  
  if (nrow(donnees) < 1) 
    return(graphique_pas_de_donnees())
  
  format <- format_num(format = ",.2~f", locale = "fr-FR", suffix = suffix)
  
  apex(
    data = donnees,
    type = "radar",
    mapping = aes(x = donnees[[variable_x]], y = donnees[[variable_y]])
  ) %>% 
    ax_labs_batistato(
      titre = titre,
      sous_titre = sous_titre,
      y = label_y
    ) %>% 
    ax_tooltip(y = list(formatter = format,
                        title = list(
                          formatter = JS("function() {return 'Valeur :';}")
                        ))) %>% 
    ax_colors(palette_drieat("cumulus")) %>% 
    ax_legend(showForSingleSeries = TRUE) %>% 
    ax_copyright_batistato()
}



graphique_radar_groupe <- function(donnees,
                                   titre = NULL,
                                   sous_titre = NULL,
                                   variable_x,
                                   variable_y,
                                   groupe = NULL,
                                   suffix = NULL,
                                   height = NULL,
                                   label_y = NULL) {
  
  
  if (nrow(donnees) < 1) 
    return(graphique_pas_de_donnees())
  
  format <- format_num(format = ",.2~f", locale = "fr-FR", suffix = suffix)
  
  apex(
    data = donnees,
    type = "radar",
    mapping = aes(x = donnees[[variable_x]], y = donnees[[variable_y]], group = donnees[[groupe]]),
    height = height
  ) %>% 
    ax_plotOptions(
      radar = list(size = 300)
    ) %>% 
    ax_labs_batistato(
      titre = titre,
      sous_titre = sous_titre,
      y = label_y
    ) %>% 
    ax_tooltip(
      y = list(formatter = format)
    ) %>% 
    ax_markers(
      size = 4,
      # colors = list("#FFF"),
      strokeWidth = 2,
    ) %>%
    ax_fill(opacity = 0) %>% 
    ax_legend(showForSingleSeries = TRUE) %>% 
    ax_chart(defaultLocale = "fr", animations = list(enabled = FALSE)) %>% 
    ax_colors(c(palette_drieat("cumulus"), palette_drieat("menthe"), palette_drieat("archipel"), palette_drieat("emeraude"))) %>% 
    ax_copyright_batistato()
}



graphique_line <- function(donnees,
                           titre,
                           sous_titre,
                           variable_x,
                           variable_y,
                           tooltip_y = "Consommation: ",
                           suffix = NULL,
                           texte_pas_de_donnees = "Pas de données pour ce territoire.") {
  if (nrow(donnees) < 1) 
    return(graphique_pas_de_donnees(texte = texte_pas_de_donnees))

  apex(
    data = donnees,
    type = "line",
    mapping = aes(x = donnees[[variable_x]], y = donnees[[variable_y]]),
    height = calculer_hauteur_graphique_barres(donnees[[variable_x]])
  ) %>%
    ax_chart(
      animations = list(enabled = FALSE),
      defaultLocale = "fr",
      toolbar = list(
        tools = list(
          zoom = FALSE,
          zoomin = FALSE,
          zoomout = FALSE,
          reset = FALSE,
          pan = FALSE
        )
      )
    ) %>% 
    ax_labs_batistato(
      titre = titre,
      sous_titre = sous_titre
    ) %>% 
    ax_tooltip(
      y = list(
        formatter = format_num(format = ",.2~f", locale = "fr-FR", suffix = suffix),
        title = list(
          formatter = JS(
            sprintf(
              "function() {return '%s';}",
              tooltip_y
            )
          )
        )
      )
    ) %>% 
    ax_yaxis(
      labels = list(
        formatter = format_num(format = ",.2~f", locale = "fr-FR")
      )
    ) %>% 
    # ax_colors_manual(couleurs_modalite(donnees[[groupe]])) %>% 
    ax_legend(showForSingleSeries = TRUE) %>% 
    ax_yaxis(
      labels = list(
        maxWidth = 500
      )
    ) %>% 
    ax_legend(show = FALSE) %>% 
    ax_copyright_batistato()
}
