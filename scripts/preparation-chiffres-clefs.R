#  ------------------------------------------------------------------------
#
# Title : Batistato - Préparation chiffres-clefs 
#    By : DRIEAT
#  Date : 2023-03
#
#  ------------------------------------------------------------------------


## Table des correspondances des chiffres-clefs

# nrj_1	: "Vue d’ensemble – Chiffre clé – Consommation du territoire en MWh"
# nrj_2	: "Vue d’ensemble – Chiffre clé – Equivalent en production photovoltaïque"
# nrj_3	: "Vue d’ensemble – Chiffre clé – Equivalent en production réacteur nucléaire"
# nrj_4	: "Vue d’ensemble – Chiffre clé – Consommation de chauffage en MWh"
# log_1	: "Vue d’ensemble – Chiffre clé – % de logements construits individuels privés construits avant 1990"
# log_2	: "Vue d’ensemble – Chiffre clé – % de logements collectifs privés construits avant 1990"
# log_3	: "Vue d’ensemble – Chiffre clé – % de logements du parc social construits avant 1990"
# ter_1	: "Vue d’ensemble – Chiffre clé – Surfaces assujetties au décret EET"


# Packages ----------------------------------------------------------------

library(readr)
library(dplyr)
library(janitor)
library(tidyr)



# Donnees -----------------------------------------------------------------

bat_ch_clef <- read_csv("inputs/bat_chiffres_clef.csv", na = "NULL")
bat_ch_clef <- janitor::clean_names(bat_ch_clef)

chiffres_clefs <- bat_ch_clef %>% 
  select(
    maille, idcom, siren_epci, dep_com,
    # Chiffres-clefs accueil et radar
    conso_territoire = nrj_2,
    conso_equivalent_photovoltaique = nrj_4,
    conso_equivalent_nucleaire = nrj_5,
    conso_chauffage = nrj_6,
    pourcentage_logements_individuels = log_1,
    pourcentage_logements_collectifs = log_2,
    pourcentage_logements_parc = log_3,
    surfaces_assujetties = ter_1,
    conso_territoire_radar = nrj_3,
    conso_chauffage_radar = nrj_10,
    pourcentage_logements_radar = log_5,
    nombre_locaux_assujettis_radar = ter_4,
    nombre_logements_dpe_radar = dpe_2
  ) %>% 
  mutate(
    across(5:last_col(), as.numeric)
  ) %>% 
  unite("code_territoire", c(idcom, dep_com, siren_epci), na.rm = TRUE) 
chiffres_clefs$code_territoire[chiffres_clefs$maille == "REGION"] <- "11"


# Enregistrement ----------------------------------------------------------

saveRDS(chiffres_clefs, file = "datas/chiffres_clefs.rds")
